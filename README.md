# Whitelist
## 1. Deployed Whitelist contract address(sepolia)
0x435f7Ff10902573bD14322e5eeDC44aD2A7d86a2

## 2. verify Whitelist contract
npx hardhat verify --constructor-args arguments.js 0x435f7Ff10902573bD14322e5eeDC44aD2A7d86a2 --network sepolia


# NFT-Collection
## 1. contract address(sepolia)
0xaD8E4d3b1cD6e6947322eE9A722cdCC8f4d6F200

## 2. deploy contract
npx hardhat run scripts/deploy.js --network sepolia

## 3. verify contract
npx hardhat verify --network sepolia 0xaD8E4d3b1cD6e6947322eE9A722cdCC8f4d6F200 --constructor-args arguments.js


# ICO
# 1. Crypto Devs Token Contract Address:
0xb81C8c2E24e1F7965574F8a2c4637de58510e2Db

## 2. verify contract
npx hardhat verify --constructor-args arguments.js 0xb81C8c2E24e1F7965574F8a2c4637de58510e2Db --network sepolia


# (DAO) Decentralize autonomous organization
FakeNFTMarketplace deployed to:  0xB41A0cBf66482132A5488BA317a78fD14C393467
CryptoDevsDAO deployed to:  0x8936599C26EfF5F4d86ec698DA32598df126260A

## 2. verify contract
npx hardhat verify --constructor-args arguments/CryptoDevsDAO.js 0x8936599C26EfF5F4d86ec698DA32598df126260A --network sepolia

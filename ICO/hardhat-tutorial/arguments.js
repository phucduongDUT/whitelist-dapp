const { CRYPTO_DEVS_NFT_CONTRACT_ADDRESS } = require("./constants");

module.exports = [
    CRYPTO_DEVS_NFT_CONTRACT_ADDRESS
];
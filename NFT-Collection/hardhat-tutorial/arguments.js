const { WHITELIST_CONTRACT_ADDRESS, METADATA_URL } = require("./constants");

module.exports = [
    METADATA_URL,
    WHITELIST_CONTRACT_ADDRESS
];
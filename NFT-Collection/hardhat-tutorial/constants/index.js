// Address of the Whitelist Contract that you deployed
const WHITELIST_CONTRACT_ADDRESS = "0x435f7Ff10902573bD14322e5eeDC44aD2A7d86a2";
// URL to extract Metadata for a Crypto Dev NFT
const METADATA_URL = "https://whitelist-dapp-one-topaz.vercel.app/api/";

module.exports = { WHITELIST_CONTRACT_ADDRESS, METADATA_URL };